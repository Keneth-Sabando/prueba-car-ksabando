import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { CanActivate } from "@angular/router";

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate() {
    const valorRecuperado = localStorage.getItem('clave');
    return true;
    if (valorRecuperado != null) {
      console.log("No estás logueado");
      this.router.navigate(["/login"]);
      return false;
    }

    return true;
  }
}
