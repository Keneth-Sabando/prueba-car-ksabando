export interface Productos {

    id: String,
    descripcion: String,
    precio: String,
    estado: String,
    detalle: String,
    imagen: String
}
