export interface Plan {
  nombre: string;
  icono: string;
  descripcion: string;
  valor: string;
  frecuencia: string;
  codigo: string;
  plan: number;
  productosPlan: ProductoPlan[];
}

export interface ProductoPlan {
  producto: number;
  descripcion: string;
  cantidad: number;
}
