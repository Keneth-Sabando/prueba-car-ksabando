import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseDto } from 'src/app/shared/Interface/Response.dto';


@Injectable({
  providedIn: 'root'
})

export class ServiceProductsService {
  private URL = "https://localhost:44364/api/producto";

  constructor(public http: HttpClient) {

  }


  insertArchivo(data?: any) {
    return this.http.post(this.URL, data);
  }


  obtenerProductos( params?: any) {
    return this.http.get<ResponseDto>(this.URL)
  }
}
