import { Component } from '@angular/core';
import { ServiceProductsService } from '../../services/service-products.service';
import { Productos } from 'src/app/core/interface/Productos.interface';
import { ResponseDto } from 'src/app/shared/Interface/Response.dto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent {
  products?: Productos[]; 

  constructor(private _service: ServiceProductsService) { }


  ngOnInit(): void {
    this.obtenerCostosProyectosComposite();
    ;
  }

  public filtro(): void{
    this.products = this.products?.filter( (p) => Number(p.precio) < 10);
    console.log(this.products)
  }


  private obtenerCostosProyectosComposite() {
    this._service.obtenerProductos().subscribe({
      next: (res: ResponseDto) => {
          this.products = res.productos;
          console.log(this.products);
      },
      error: (error: any) => {
        console.error(error)
      }
    });
  }
}
