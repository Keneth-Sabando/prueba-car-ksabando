import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceProductsService } from '../../services/service-products.service';
import { Productos } from 'src/app/core/interface/Productos.interface';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent {
  producto?: Productos;

  id: string | undefined = '';
  productForm: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private _service: ServiceProductsService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => this.id = params['id'])
  }


  ngOnInit(): void {
    console.log(this.id)
    if (this.id != undefined) {
      this.productForm = this.formBuilder.group({
        id: [1], // Puedes establecer aquí el valor real del id
        name: ['Clothes', Validators.required],
        image: ['https://picsum.photos/640/640?r=829'],
        createdAt: ['2023-09-21T00:39:22.000Z'],
        updatedAt: ['2023-09-21T00:39:22.000Z']
      });
    } else {
      this.productForm = this.formBuilder.group({
        id: [],
        name: [],
        image: [],
        createdAt: [],
        updatedAt: []
      });
    }

  }

  onSave(): void {
    // Accede a las propiedades de los controles de forma segura usando el operador "?"
    const name = this.productForm.get('name')?.value;
    const image = this.productForm.get('image')?.value;
    const createdAt = this.productForm.get('createdAt')?.value;
    const updatedAt = this.productForm.get('updatedAt')?.value;

    // Implementa aquí la lógica para guardar los datos del formulario
    // Puedes verificar si las propiedades son null o undefined si es necesario
    if (name !== null && image !== null && createdAt !== null && updatedAt !== null) {
      // Realiza la acción de guardar aquí
      this._service.insertArchivo(this.productForm.getRawValue()).subscribe({
        next: (resp: any) => {
          console.log(resp);
        }
      });
      console.log(name, image, createdAt, updatedAt);
    } else {
      alert("Datos Vacios")
    }
  }
}
