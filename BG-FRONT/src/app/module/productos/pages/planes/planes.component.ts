import { Component } from '@angular/core';
import { Plan } from 'src/app/core/interface/Plan.interface';

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.css']
})
export class PlanesComponent {
  plan: Plan = {
    nombre: "Plan Estudiantil",
    icono: "plan3.jpg",
    descripcion: "¿Necesitas más tiempo para estudiar y tienes ropa que lavar? Este plan es para ti",
    valor: "$21.00",
    frecuencia: "",
    codigo: "001",
    plan: 6,
    productosPlan: [
      {
        producto: 64,
        descripcion: "FUNDA DE LAVANDERÍA",
        cantidad: 2
      }
    ]
  };

  constructor() { }

  ngOnInit(): void {
    // Supongamos que obtienes los datos del plan de alguna fuente, como una API.
    this.plan = {
      nombre: "Plan Estudiantil",
      icono: "plan3.jpg",
      descripcion: "¿Necesitas más tiempo para estudiar y tienes ropa que lavar? Este plan es para ti",
      valor: "$21.00",
      frecuencia: "",
      codigo: "001",
      plan: 6,
      productosPlan: [
        {
          producto: 64,
          descripcion: "FUNDA DE LAVANDERÍA",
          cantidad: 2
        }
      ]
    };
  }
}
