import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { servicelogin } from '../../services/servicelogin.service';
import { LoginRequest } from '../../Interface/Login.interface';
import { ResponseLogin } from '../../Interface/Response.dto';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  form!: FormGroup;

  response :any;

  constructor(private formBuilder: FormBuilder, 
    private router: Router, private loginService:servicelogin,
    ) {
    
  }

  ngOnInit(): void{
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      contrasena: ['', Validators.required]
    });
  }

  onSubmit() {
      if(this.form.valid){
        
        this.loginService.obtenerLogin({username: this.form.value.username , contrasena: this.form.value.contrasena}).subscribe({
          next:(resp:ResponseLogin)=>{
            if(resp.success){
              alert("Se ha logeado exitosamente");
              this.router.navigate(["/productos"]);
            }else{
              alert("Las credenciales no son validas");
            }
          },
          error: (err)=>{
            console.error(err);
          }
        })

      }else{
        console.log("error de llenado de datos");
      }
  }

}
