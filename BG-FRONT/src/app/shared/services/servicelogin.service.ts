import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseLogin } from '../Interface/Response.dto';
import { LoginRequest } from '../Interface/Login.interface';


@Injectable({
  providedIn: 'root'
})

export class servicelogin {

  private URL = "https://localhost:44364/api/usuario";  

  constructor(public http: HttpClient) {

  }

  obtenerLogin( dtoLogin : LoginRequest) {
    return this.http.post<ResponseLogin>(this.URL, dtoLogin);
  }
}
