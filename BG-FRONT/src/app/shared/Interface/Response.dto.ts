import { Productos } from "src/app/core/interface/Productos.interface";

export interface ResponseDto {

      success: number,
      productos: Productos[]
}

export interface ResponseLogin {

      success?: number,
      usuario?: string
}

