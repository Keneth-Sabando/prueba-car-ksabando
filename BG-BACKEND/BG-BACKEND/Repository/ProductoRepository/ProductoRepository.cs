﻿using BG_BACKEND.Modelos.Producto;
using BG_BACKEND.Utils;
using System.Data;
using System.Data.SqlClient;

namespace BG_BACKEND.Repository.ProductoRepository
{
    public class ProductoRepository
    {


        public ProductoResponse obtenerProductos()
        {
            List<Producto> productList = new List<Producto>();

            Conexion cn = new Conexion();

            using (var conexion = new SqlConnection(cn.getCadenaSQL()))
            {
                conexion.Open();

                SqlCommand cmd = new SqlCommand("spProducto", conexion);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        productList.Add(new Producto()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Descripcion = dr["Descripcion"].ToString(),
                            Precio = Convert.ToDecimal(dr["Precio"]),
                            Estado = dr["Estado"].ToString(),
                            Detalle = dr["Detalle"].ToString(),
                            Imagen = dr["Imagen"].ToString()
                        });
                    }
                }

                conexion.Close();

            }

            ProductoResponse productoResponse = new ProductoResponse();
            productoResponse.Success = productList != null ? 1 : 0;
            productoResponse.Productos = productList;

            return productoResponse;
        }

    }
}
