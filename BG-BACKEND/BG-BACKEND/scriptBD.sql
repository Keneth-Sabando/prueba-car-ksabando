
--CREATE DATABASE bgBackend;

USE bgBackend;

--------------------------------------------------------------------------
-- USUARIO
--------------------------------------------------------------------------
CREATE TABLE Usuario (
    id INT IDENTITY(1,1) PRIMARY KEY,
    username VARCHAR(255) NOT NULL unique,
    contrasena VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    [plan] INT NULL,
    telefono VARCHAR(20) NULL
);


INSERT INTO Usuario (username, contrasena, nombre, [plan], telefono)
VALUES
    ('juanp', 'contrasena1', 'Juan P�rez', 1, '0956321365'),
    ('mariag', 'contrasena2', 'Mar�a Garc�a', 2, '0926321363'),
    ('luisg', 'contrasena3', 'Luis Gonz�lez', NULL, '0946321361'),
    ('anam', 'contrasena4', 'Ana Mart�nez', 3, '0976361362'),
    ('pedror', 'contrasena5', 'Pedro Rodr�guez', NULL, '0966999365');



--------------------------------------------------------------------------
-- PRODUCTO
--------------------------------------------------------------------------

CREATE TABLE Producto (
    id INT IDENTITY(1,1) PRIMARY KEY,
    descripcion VARCHAR(255) NOT NULL,
    precio DECIMAL(6, 2) NOT NULL,
    estado CHAR NOT NULL,
    detalle VARCHAR(255) NOT NULL,
    imagen VARCHAR(255) NOT NULL
);


-- Insertar 10 registros de ejemplo en la tabla Producto (ropa)
INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES
    ('Camiseta de algod�n blanca', 19.99, 'N', 'Camiseta de manga corta, talla M', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Pantalones vaqueros azules', 39.99, 'N', 'Jeans de estilo cl�sico, talla 32x32', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Vestido floral', 49.99, 'N', 'Vestido de flores, talla S', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Chaqueta de cuero negra', 79.99, 'N', 'Chaqueta de cuero genuino, talla L', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Zapatos deportivos blancos', 59.99, 'N', 'Zapatillas deportivas blancas, talla 9', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Bufanda de lana roja', 14.99, 'N', 'Bufanda de lana suave, color rojo', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Blusa de seda negra', 29.99, 'N', 'Blusa elegante de seda, talla M', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Sombrero de paja', 9.99, 'N', 'Sombrero de paja natural, tama�o ajustable', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Calcetines de algod�n a rayas', 7.99, 'N', 'Calcetines c�modos a rayas, talla �nica', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000'),
    ('Abrigo de invierno gris', 99.99, 'N', 'Abrigo abrigado de invierno, talla XL', 'https://superexitoec.vteximg.com.br/arquivos/ids/159757-500-500/3350010418-4.jpg?v=638103607787570000');



--------------------------------------------------------------------------
CREATE PROCEDURE spLogin
    @username VARCHAR(255) = NULL,
    @contrasena VARCHAR(255) = NULL
AS
BEGIN

    DECLARE @isValid int = 0;

    IF EXISTS (SELECT 1 
			   FROM Usuario 
			   WHERE username = @username AND contrasena = @contrasena)
        SET @isValid = 1;

    SELECT @isValid AS isValid;

END;



--------------------------------------------------------------------------
CREATE PROCEDURE spProducto
AS
BEGIN
    
	SELECT * FROM Producto;
        
END;

