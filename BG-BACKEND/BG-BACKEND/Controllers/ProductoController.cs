﻿using BG_BACKEND.Modelos.Producto;
using BG_BACKEND.Repository.ProductoRepository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BG_BACKEND.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        
        
        private readonly ProductoRepository _repository = new ProductoRepository();


        [HttpGet]
        [ProducesResponseType(typeof(ProductoResponse), 200)]
        public async Task<IActionResult> ObtenerProductos()
        {
            ProductoResponse productoResponse = _repository.obtenerProductos();
            return Ok(productoResponse);
        }
    }
}
